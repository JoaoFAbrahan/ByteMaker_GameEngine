# ByteMaker_GameEngine
Este projeto tem como objetivo o estudo e desenvolvimento de uma Game Engine utilizando as APIs Vulkan, DirectX e Metal, com o intuito principal de compreender e implementar os conceitos propostos pela Computação Gráfica, assim como os conceitos de desenvolvimento de softwares multiplataforma.


## Buildando o Projeto
Para executar o projeto o usuário deverá fornecer as bibliotecas auxiliares via VCPKG utilizadas na aplicação, por padrão as bibliotecas não estão vinculadas no repositório do projeto e podem ser adicionadas de duas formas.
- 1. Através do sistema de automatização do VCPKG vinculado neste repositório, para isso abra o CMakeLists.txt do diretório raiz e na `LINHA 12` mude o valor da variável `AUTOMATE_VCPKG` de `OFF` para `ON` e salve as alterações. 
- 2. Adicionando manualmente o path do reposítorio VCPKG, e baixando os packages manualmente. Para adicionar o path do repositório abra o CMakeLists.txt do diretório raiz, garanta que o valor da variável `AUTOMATE_VCPKG` na `LINHA 12` esteja setado em `OFF` e depois adicione o caminho através da `LINHA 44` e salve as alterações.

 **OBS**: Para adição manual do repositório VCPKG, deve-se garantir que os sequintes packages estejam instalados:
- sdl2
- imgui
- d3dx12 *(Para OS Windows)*
- vulkan *(Para OS Linux)* 

## IDE e Configurações
A IDE utilizada para desenvolvimento desse projeto foi o *Visual Studio Code*, com os seguintes compiladores:
- MinGW (Windows), 
- GCC (Linux),
- Clang (MacOS).


## Configuração de Automatização para o Visual Studio Code
Exemplo de código para configuração de automação dos processos de compilação no Visual Studio Code, tanto para o modo Debug, quanto para o Release

- launch.json
```
{
    "version": "2.0.0",
    "configurations": 
    [
        {   
            "name": "Debug Program",
            "type": "cppdbg",
            "request": "launch",      
            "windows": 
            {
                "name": "Launch Program",
                "type": "cppdbg",
                "request": "launch",
                "program": "${workspaceFolder}/build/Debug/bin/RenderEX_Crossplatform.exe",
                "args": [],
            },
            "linux": 
            {
                "name": "Debug Program",
                "type": "cppdbg",
                "request": "launch",
                "program": "${workspaceFolder}/build/Debug/bin/RenderEX_Crossplatform",
                "args": [],
            },
            "stopAtEntry": false,
            "cwd": "${workspaceFolder}"
        }
    ]
}
```

- User/settings.json
```
"cmake.buildDirectory": "${workspaceFolder}/build/${buildType}",
```


## APIs complementares utilizadas
- [VCPKG](https://vcpkg.io/en/index.html)
- [CMake](https://cmake.org/) 
- [SDL](https://www.libsdl.org/)
- [Dear ImGUI](https://github.com/ocornut/imgui) 


## Autores
- João Felipe Abrahan
- Jorge Thomas Constantino Calligopoulo


## Licença
Este projeto esta licenciado com os termos de licença MIT.


## Status do Projeto
Este projeto está atualmente em fase de Desenvolvimento e sem prazo de finalização.
